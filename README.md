This project use the docker-privileged shared runners to make a install.tar.gz tarball from docker image.

See [KB0005851](https://cern.service-now.com/service-portal/article.do?n=KB0005851)
for details about the docker-privileged shared runners.